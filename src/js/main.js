requirejs.config({
    baseUrl: '/js/plugins',
    paths: {
        jquery: 'jquery.js'
    }
});

require(['js-init', 'tab-actions'], function (JsInit, TabActions) {
    'use strict';
    
    /* 1 - Remove the hidden class and no-js message if javascript is enablec*/
    JsInit.removeNoJsMarkup();

    /* 2 - Init tab module that listens the click events */
    TabActions.init();

});