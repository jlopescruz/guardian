define('tab-actions', ['jquery'], function ($) {
    'use strict';

    /* Module constructor - Variables */
    function TabActions () {
        this.$appWrapper       = $('.app-wrapper');
        this.$tab              = this.$appWrapper.find('nav a');
        this.$resultsList      = this.$appWrapper.find('ol');
        this.$spinner          = this.$appWrapper.find('.spinner');
        this.GUARDIAN_API_KEY  = 'fgdz5f55mmgfrbnqtw3t758q';
        this.activeTab         = 'news';
    }

    /* Module public method */
    TabActions.prototype.init = function () {
        var that = this;

        function tabClick (mouseEvent) {
            var $this = $(this);

            mouseEvent.preventDefault();
            
            that.$tab.parent().removeClass('active');
            $this.parent().addClass('active');
            that.showResults($this.attr('data-type'));
            that.$resultsList.empty();
        }

        this.$tab.on('click', tabClick);
        this.loadFirstResults();
    };

    /* Fire first tab results on page load */
    TabActions.prototype.loadFirstResults = function () {
        this.$tab.eq(0).click();
    };

    /* Function that handles the different data requests */
    TabActions.prototype.showResults = function (type) {
        var that = this;

        this.$spinner.addClass('visible');

        /* TODO - Handle error handling */
        switch (type) {
            case 'news':
                this.activeTab = 'news';
                this.fetchData('uk+news').then(function(data) {
                    that.populateList(data);
                });
            break;

            case 'football':
                this.activeTab = 'football';
                this.fetchData('football').then(function(data) {
                    that.populateList(data);
                });
            break;

            case 'travel':
                this.activeTab = 'travel';
                this.fetchData('travel').then(function(data) {
                    that.populateList(data);
                });
            break;
        }
    };

    /* Promises based generic method that hits Guardian API */
    TabActions.prototype.fetchData = function (query) {
        var deffered      = $.Deferred(),
            that          = this;

        /* TODO  Cache content? Is the content likely to change frequently? */

        $.ajax({
                url: 'http://beta.content.guardianapis.com/tags?q=' + query +'&api-key=' + that.GUARDIAN_API_KEY,
                success: function (data) {
                    deffered.resolve(data);
                },
                error : function (data) {
                    deffered.reject(data);
                }

        });

        return deffered.promise();
    };

    /* Populates the DOM ol with data coming from the API */
    TabActions.prototype.populateList = function (data) {
        this.$spinner.removeClass('visible');

        /* TODO - Handle empty responses */
        this.$resultsList.append(this.buildListHTML(data.response.results));
    };

    /* Function responsible for the markup */
    TabActions.prototype.buildListHTML = function (results) {
        var htmlOutput = '',
            i;
        
        /* NOTE - API IS NOT RETURNING PROPERTY trailText */
        for (i = 0; i < results.length; i++) {
            htmlOutput += '<li>';
            htmlOutput += '<a target="_blank" href="' + results[i].webUrl + '">' + results[i].webTitle + '</a>';
            htmlOutput += '</li>';
        }

        return htmlOutput;
    };

    return new TabActions();
});