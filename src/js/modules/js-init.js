define('js-init', ['jquery'], function ($) {
    'use strict';

    function JsInit () {
        this.$appWrapper  = $('.app-wrapper');
        this.$noJsMessage = this.$appWrapper.find('h2');
    }

    JsInit.prototype.removeNoJsMarkup = function () {
        this.$appWrapper.removeClass('hidden-content');
        this.$noJsMessage.addClass('hidden');
    };

    return new JsInit();
});